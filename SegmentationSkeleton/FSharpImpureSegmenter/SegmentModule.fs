﻿module SegmentModule

open System



type Coordinate = (int * int) // x, y coordinate of a pixel
type Colour = byte list       // one entry for each colour band, typically: [red, green and blue]

type Segment = 
    | Pixel of Coordinate * Colour
    | Parent of Segment * Segment 


let rec grabRed tree : float list =
    

    match tree with
    | Pixel (coord, colour) -> 
        
        float(colour.[0])::[]

    | Parent(left, right) -> 

        List.append (grabRed left) (grabRed right)
        
let rec grabGreen tree : float list =
       
    match tree with
    | Pixel (coord, colour) -> 
        
        float(colour.[1])::[]

    | Parent(left, right) -> 

        List.append (grabGreen left) (grabGreen right)
        
let rec grabBlue tree : float list =
    
    match tree with
    | Pixel (coord, colour) -> 
        
        float(colour.[2])::[]

        
    | Parent(left, right) -> 

        List.append (grabBlue left) (grabBlue right)




        
                

let sqr x = x * x

let stdDev nums =

    let mean = nums |> List.average

    let variance = nums |> List.averageBy (fun x -> sqr(x - mean))

    sqrt(variance)

// return a list of the standard deviations of the pixel colours in the given segment
// the list contains one entry for each colour band, typically: [red, green and blue]
let stddev (segment: Segment) : float list =

    //System.Console.WriteLine("Test");
    //let shit = []
    let red = grabRed segment
    let green = grabGreen segment
    let blue = grabBlue segment
    //System.Console.WriteLine(printf "all the reds %A" red) 
    //System.Console.WriteLine(printf "all the blues %A" green) 
    //System.Console.WriteLine(printf "all the greens %A" blue) 

    let list1 = [stdDev red]
    let list2 = [stdDev blue]
    let list3 = [stdDev green]
    let final = List.append list1 (List.append list3 list2)
    //System.Console.WriteLine(printf "SD %A" final)
    final
   



// determine the cost of merging the given segments: 
// equal to the standard deviation of the combined the segments minus the sum of the standard deviations of the individual segments, 
// weighted by their respective sizes and summed over all colour bands
let mergeCost segment1 segment2 : float = 

    // Merge the two segments
    let seg3 = Parent(segment1, segment2)

    // Determine the standard deviations
    let sd1 = List.sum(stddev segment1) * float((grabRed segment1).Length)
    let sd2 = List.sum(stddev segment2) * float((grabRed segment2).Length)
    let sd3 = (List.sum(stddev seg3)) * (float((grabRed seg3).Length))

    // combine
    let combinedSD = sd1+sd2
 
    float(sd3-combinedSD)
