﻿module Program


open SegmentModule
open TiffModule
open DitherModule
open SegmentationModule

[<EntryPoint>]
let main argv =
    // load a Tiff image
    //////System.Console.WriteLine(argv.[0])
    //let image = TiffModule.loadImage argv.[0]
    let image = TiffModule.loadImage "C:\\Users\\Jarrden\\source\\repos\\402-assignment\\SegmentationSkeleton\\TestImages\\L15-3792E-1717N-Q4.tif"


    // testing using sub-image of size 32x32 pixels
    let N = 5

    // increasing this threshold will result in more segment merging and therefore fewer final segments
    let threshold = 800.0

    ////System.Console.WriteLine("Doing it all \n\n\n")

    //let tiffImage1 = TiffModule.createImage 2 2 [|0xFF528843;0xFF45A69B;0xFF023B80;0xFF286F6C;|]

   // let segmentation = SegmentationModule.segment tiffImage1 1 100.000000

    // determine the segmentation for the (top left corner of the) image (2^N x 2^N) pixels
    let segmentation = SegmentationModule.segment image N threshold

    // draw the (top left corner of the) original image but with the segment boundaries overlayed in blue
    TiffModule.overlaySegmentation image "segmented.tif" N segmentation

    0 // return an integer exit code