﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace CSharpSegmenter
{
    class Segment
    {
        public Segment parent;
        public Segment rootParent;
        public List<Pixel> pixelList;


        // Returns a list of pixels from the given segment
        // This goes through each of the segment's potential children and returns all pixels
        public List<Pixel> returnPixels(Segment parent)
        {
            List<Pixel> pixelList = new List<Pixel>();

            // If the segment is a parent, go through its children and return pixels
            if (parent.GetType() == typeof(Parent))
            {
                Parent theParent = (Parent)parent;
                if (theParent.child1.GetType() == typeof(Parent))
                {
                    pixelList.AddRange(returnPixels(theParent.child1));
                }
                else
                {
                    pixelList.Add((Pixel)theParent.child1);
                }


                if (theParent.child2.GetType() == typeof(Parent))
                {
                pixelList.AddRange(returnPixels(theParent.child2));
                }
                else
                {
                    pixelList.Add((Pixel)theParent.child2);
                }
            }

            // If it's not a parent, add it to the list
            else
            {
                pixelList.Add((Pixel)parent);
            }
            return pixelList;
        }

        // Goes through each of the segment's children and updates their root Parent
        public void UpdateRoot(Parent NewRoot, Segment segment)
        {
            // If segment is a parent, update parent's and children's root segment
            if (segment.GetType() == typeof(Parent))
            {
                Parent parentSegment = (Parent)segment;
                parentSegment.rootParent = NewRoot;
                UpdateRoot(NewRoot, parentSegment.child1);
                UpdateRoot(NewRoot, parentSegment.child2);
            }

            // If child, update its root
            else
            {
                segment.rootParent = NewRoot;
            }


        }

        // Calculates the standard deviation for each of the colours in a segment
        // Returns a list of standard deviations
        public static List<float> Stddev(Segment segment)
        {

            // Obtain a list of pixels for the given segment
            List<Pixel> pixelList;

            if(segment.GetType() == typeof(Parent))
            {
                Parent parent = (Parent)segment;
                pixelList = parent.returnPixels(segment);
            }
            else
            {
                Pixel pixel = (Pixel)segment;

                pixelList = new List<Pixel>();
                pixelList.Add(pixel);
            }


            // Obtain a list of a list of colours
            List<List<int>> colours = new List<List<int>>();
            int[,] colourList = new int[3, 512];


            // Go through each of the pixels and add their colours to the appropriate list
            foreach (Pixel pixel in pixelList)
            {
                for (int i = 0; i <= pixel.colour.Length - 1; i++)
                {
                    if (colours.Count > i)
                    {
                        colours[i].Add(pixel.colour[i]);
                    }
                    else
                    {
                        colours.Add(new List<int>());
                        colours[i].Add(pixel.colour[i]);
                    }


                }

            }


            // Calculate the standard deviation for each of the colour lists
            List<float> stddevList = new List<float>();

            foreach (List<int> colour in colours)
            {
                double avg = colour.Average();
                stddevList.Add((float)Math.Sqrt(colour.Average(v => Math.Pow(v - avg, 2))));
            }

            return stddevList;           
        }

        // Returns the amount of pixels in a given segment
        public static int pixelCount(Segment segment)
        {

            if(segment.GetType() == typeof(Parent))
            {
                Parent parent = (Parent)segment;
                return parent.returnPixels(parent).Count;
            }

            // If segment is not a parent, return 1 as its a Pixel
            else
            {
                return 1;
            }
        }

        
        // Determine the cost of merging two segments
        public static float mergeCost(Segment seg1, Segment seg2)
        {
            // Create a new segment of the two segments that will potentially be merged
            Parent seg3 = new Parent(seg1.rootParent, seg2.rootParent, true);

            // Determine the standard deviations of their colours
            List<float> stdv1 = Stddev(seg1);
            List<float> stdv2 = Stddev(seg2);
            List<float> stdv3 = Stddev(seg3);

            // Calculate the standard deviation of each segment
            float sum1 = stdv1.Sum();
            float sd1 = sum1 * pixelCount(seg1);

            float sum2 = stdv2.Sum();
            float sd2 = sum2 * pixelCount(seg2);

            float sum3 = stdv3.Sum();
            float sd3 = Stddev(seg3).Sum() * pixelCount(seg3);


            // Combine the cost of the original two segments
            float sd12 = sd1 + sd2;

            // Return the cost of the two segments minus the new one
            return sd3 - sd12;

        }

    }

    class Parent : Segment
    {
        public Segment child1;
        public Segment child2;
        

        // Creates a new parent
        // Fake refers to if the segment is actually created or just used to determine mergecost
        public Parent(Segment seg1, Segment seg2, bool fake = false)
        {

            // generate the list of pixels in this given segment
            pixelList = new List<Pixel>();
            pixelList.AddRange(returnPixels(seg1));
            pixelList.AddRange(returnPixels(seg2));


            // If this segment is not made to determine merge cost, update the segment's children
            if (!fake) {
                this.parent = this;
                this.rootParent = this;

                seg1.parent = this;
                seg2.parent = this;
                UpdateRoot(this, seg1);
                UpdateRoot(this, seg2);
            }
            
            // Set the segment's children
            child1 = seg1;
            child2 = seg2;

        }   

    }



    class Pixel : Segment
    {

        public int x;
        public int y;
        public byte[] colour;

        // Create a new Pixel
        public Pixel(int x, int y, byte[] colour)
        {
            this.x = x;
            this.y = y;
            this.colour = colour;
            this.pixelList = new List<Pixel>();
            this.parent = this;
            this.rootParent = this;
            pixelList.Add(this);
        }


    }

    

}
