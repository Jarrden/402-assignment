﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CSharpSegmenter
{
    class Program
    {


        public static Pixel[,] globePix;  // change that to correlate with N

        public static Segment finalSegment;

        public static Segment grabSegment(int x, int y)
        {
            if (x < 0 || x > 32 || y < 0 || y > 32)
            {
                return new Segment();
            } else { 

                return globePix[x, y].rootParent;
            }

        }


        static void Main(string[] args)
        {

            TiffImage image = new TiffImage("C:\\Users\\Jarrden\\source\\repos\\402-assignment\\SegmentationSkeleton\\TestImages\\L15-3792E-1717N-Q4.tif");

            // Max size for image
            int N = 5;
            int max = 1 << N;

            // Create pixels
            globePix = new Pixel[max, max];
            createPixels(image, N);

            float threshold = 800;


            // Segment the image
            growNoChange(N, threshold);

            // Display Segmented image
            image.overlaySegmentation("Cool.tiff", N, grabSegment);

        }


        // Creates pixels for image size of N
        // Populates global pixel list with said pixels
        public static void createPixels(TiffImage image, int N)
        {

            int width = 1 << N;
            int height = 1 << N;

            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    byte[] colour = image.getColourBands(x, y);
                    Pixel newPixel = new Pixel(x, y, colour);
                    globePix[x,y] = newPixel;
                }
            }          
        }


        // Return a list of neighbouring pixels of a given pixel
        private static List<Segment> checkNeighbouringPixel(Pixel pixel, int N)
        {

            List<Segment> neighbourList = new List<Segment>();
            neighbourList.Add(pixel);

            int max = 1 << N;

            // Left
            if (pixel.x -1 >= 0)
            {
                neighbourList.Add(globePix[pixel.x - 1, pixel.y]);
            }

            // Right
            if (pixel.x + 1 <= max -1)
            {
                neighbourList.Add(globePix[pixel.x + 1, pixel.y]);
            }

            // Up
            if (pixel.y - 1 >= 0)
            {
                neighbourList.Add(globePix[pixel.x, pixel.y - 1]);
            }

            // Down
            if (pixel.y + 1 <= max-1)
            {
                neighbourList.Add(globePix[pixel.x, pixel.y + 1]);
            }

            return neighbourList;
        }

        // Find the neighbouring pixels of a given coordinate
        public static HashSet<Segment> findNeighbours(Segment segment, int N, float threshold)
        {

            // Create a list of neighbouring pixels, and pixels in a segment
            HashSet<Segment> neighbouringPixels = new HashSet<Segment>();
            List<Pixel> segmentPixel = segment.rootParent.pixelList;
            Segment rootSeg = segment;


            foreach (Pixel pixel in segmentPixel)
            {
                neighbouringPixels.UnionWith(checkNeighbouringPixel(pixel, N));//Add(checkNeighbouringPixel(pixel, N));
            }

            
            // remove the segment's pixels in order to just have the neighbouring pixels
            neighbouringPixels.ExceptWith(segmentPixel);


            // Create a new list of pixels that meet the threshold requirements
            List<Segment> thresholdMet = new List<Segment>();
            foreach (Segment nSeg in neighbouringPixels)
            {
                float cost = Segment.mergeCost(rootSeg.rootParent, nSeg.rootParent);

                if (cost <= threshold)
                {
                    thresholdMet.Add(nSeg);
                }
            }


            // Return a list of best neighbours
            float lowest = 99999;
            HashSet<Segment> lowestNeigh = new HashSet<Segment>();
            for (int i = 0; i <= 1; i++)
            {
                foreach (Segment nSeg in thresholdMet)
                {
                    float cost = Segment.mergeCost(rootSeg.rootParent, nSeg.rootParent);
                    if (cost <= lowest)
                    {
                        lowest = cost;
                    }
                    if (i == 1)
                    {
                        if(cost == lowest)
                        {
                            lowestNeigh.Add(nSeg);
                        }
                    }
                }
            }

            return lowestNeigh;


        }
        
        // Try and grow a given segment
        public static Segment TryGrow(int x, int y, int N, float threshold)
        {

            // Determine the root parent of the given coordinates and its neighbours
            Segment segment = globePix[x, y].rootParent;
            HashSet<Segment> rootNeighbours = findNeighbours(segment, N, threshold);



            foreach (Segment neighbour in rootNeighbours)
            {
                // go through each of the neighbours and grow them, if grown return the segmentation
                HashSet<Segment> neighNeighbours = findNeighbours(neighbour, N, threshold);

                foreach (Pixel pixel in segment.rootParent.pixelList)
                {
                    if (neighNeighbours.Contains(pixel))
                    {
                        Parent newParent = new Parent(pixel.rootParent, neighbour.rootParent);
                        
                        return newParent;
                    }
                }

            }


            // If unable to grow orignal segment, try grow one of its neighbours
            if (rootNeighbours.Count != 0)
            {

                Segment firstNeighbour = rootNeighbours.First();
                Pixel pixel = firstNeighbour.pixelList[(firstNeighbour.pixelList.Count - 1)];
                
                return TryGrow(pixel.x, pixel.y, N, threshold);

            }


            // If no neighbours exist, return original segment
            return segment;

        }


        // Grows all of the coordinates in the image following sequence provided by the DitherModule
        public static Segment growAllCoordinates(int N, float threshold)
        {
            
            // Grab the sequence from Dither Module
            IEnumerable<Tuple<int, int>> Sequence = Dither.coordinates(N);
            var enumerator = Sequence.GetEnumerator();


            // Move through the sequence, growing the given coordinates
            Segment final = new Segment();
            while (enumerator.MoveNext())
            {
                final = new Segment();
                int x = enumerator.Current.Item1;
                int y = enumerator.Current.Item2;
               
                final = TryGrow(x, y, N, threshold);


            }

            return final;


        }

        // Continues to grow segments of a pixel until no more segments can be grown
        public static Segment growNoChange(int N, float threshold)
        {

            Segment previous = new Segment();

            Console.WriteLine("Growing Segments...");

            while (true)
            {
                Console.WriteLine("Still Growing...");
                Segment current = growAllCoordinates(N, threshold);
                
                if (current == previous)
                {
                    Console.WriteLine("Growing Complete");
                    return current;
                }
      
                
                previous = current;

            }
        }

    




    }
}

