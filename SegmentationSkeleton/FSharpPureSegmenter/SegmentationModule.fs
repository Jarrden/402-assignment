﻿module SegmentationModule

open SegmentModule
open System.IO
open System.Linq.Expressions
open Microsoft.FSharp.Control

// Maps segments to their immediate parent segment that they are contained within (if any) 
type Segmentation = Map<Segment, Segment>

// Find the largest/top level segment that the given segment is a part of (based on the current segmentation)
let rec findRoot (segmentation: Segmentation) segment : Segment =
    
    let thing = segmentation.TryFind segment

    match thing with
    | Some x -> findRoot segmentation x
    | None -> segment



// Initially, every pixel/coordinate in the image is a separate Segment
// Note: this is a higher order function which given an image, 
// returns a function which maps each coordinate to its corresponding (initial) Segment (of kind Pixel)
let createPixelMap (image:TiffModule.Image) : (Coordinate -> Segment) =

    // Return a function that given a coordinate, returns a pixel
    fun x -> Pixel(x,TiffModule.getColourBands image x)
 

// Find the neighbouring segments of the given segment (assuming we are only segmenting the top corner of the image of size 2^N x 2^N)
// Note: this is a higher order function which given a pixelMap function and a size N, 
// returns a function which given a current segmentation, returns the set of Segments which are neighbours of a given segment
let createNeighboursFunction (pixelMap:Coordinate->Segment) (N:int) : (Segmentation -> Segment -> Set<Segment>) =
 
   // Return a function that takes a segmentation and segment
    fun segmentation segment ->
        
        let max = 1<<<N

        // Recursively return the pixels of a segment
        let rec findSegments parent : List<Coordinate> =
                
                match parent with
                |Pixel(segC, Colour) ->
                    [segC]

                |Parent(seg1, seg2) ->
                    List.append (findSegments seg1) (findSegments seg2)

        // Check the neighbouring pixels
        let left seg =
            if(((fst seg)-1 >= 0))then
                [((fst seg)-1,(snd seg))]
            else
                []
            
        let right seg =
            if(((fst seg)+1 <= max-1))then
                [((fst seg)+1,(snd seg))]
            else
                []

        let up seg = 
            if(((snd seg)-1 >= 0))then
                [((fst seg),(snd seg)-1)]
            else
                []

        let down seg = 
            if(((snd seg)+1 <= max-1))then
                [((fst seg),(snd seg)+1)]
            else
                []
        


        // return a list of neighbouring segments
        let rec findAround parent : List<Coordinate> =

            match parent with
            |Pixel(Segc, Colour) ->

                let list = [(left Segc);(right Segc);(up Segc);(down Segc);[Segc]]

                List.concat list

            |Parent(seg1, seg2) ->
                List.append (findAround seg1) (findAround seg2)

        
        let neiList = findAround segment
        let segPixList = findSegments segment

        // Create a list of Pixels that only contain the neighbours
        let segList = (Set.ofList neiList) - (Set.ofList segPixList) |> Set.toList


        // return the set of neighbours
        let rec FindSet n set: Set<Segment> =
            
            if(segList.Length - 1 >= n)then
                let seg = segList.Item n

                
                let newSet = Set.union (Set[findRoot segmentation (pixelMap((fst seg),(snd seg)))]) set
                FindSet (n + 1) newSet
            
            else
                set
                
    
        // Return the set of neighbours
        let neighbours = FindSet 0 Set.empty
        neighbours

        



// The following are also higher order functions, which given some inputs, return a function which ...


 // Find the neighbour(s) of the given segment that has the (equal) best merge cost
 // (exclude neighbours if their merge cost is greater than the threshold)
let createBestNeighbourFunction (neighbours:Segmentation->Segment->Set<Segment>) (threshold:float) : (Segmentation->Segment->Set<Segment>) =

    fun segmentation segment ->
        
        // Determine the neigbhours
        let set = neighbours segmentation segment


        // If no neighbours return an empty set
        if(set.IsEmpty)then
            Set[]        
        else

            
            let toList s = Set.fold (fun l se -> se::l) [] s

            let thing = toList set

            // Determine if the neighbours meet the merge cost
            // Return a set of those which do
            let rec grabSet n set1 = 
                if(thing.Length - 1 >= n)then
                    let seg = thing.Item n
                    let cost = mergeCost segment seg
                    if(cost <= threshold)then
                        let newSet = Set[seg]
                        Set.union newSet (grabSet (n + 1) newSet)
                    else
                        let newSet = Set.empty
                        Set.union newSet (grabSet (n + 1) newSet)               
                else
                    set1

            // Determine the neighbour(s) with the lowest mergecost and return it as a set
            let rec lowestSet n set1 lowest changed =
                 if(thing.Length - 1 >= n)then
                    let seg = thing.Item n
                    let cost = mergeCost segment seg
                    if(cost <= lowest)then
                        let newLowest = cost
                        let newSet = Set.union (Set[seg]) set1
                        lowestSet (n + 1) newSet newLowest changed
                    else
                        let newSet = set1.Remove seg
                        lowestSet (n + 1) newSet lowest changed
                 elif(not changed)then
                    lowestSet 0 set1 lowest true
                 else
                    set1

                

            let mergeCosted = grabSet 0 Set.empty

            // If no neighbours meet the threshold, return an empty set
            if(mergeCosted.Count = 0)then
                Set.empty
            else
                // Deterine and return the best neighbours
                let bestNeighbours = lowestSet 0 mergeCosted 99999.0 false         
                bestNeighbours



            

// Try to find a neighbouring segmentB such that:
//     1) segmentB is one of the best neighbours of segment A, and 
//     2) segmentA is one of the best neighbours of segment B
// if such a mutally optimal neighbour exists then merge them,
// otherwise, choose one of segmentA's best neighbours (if any) and try to grow it instead (gradient descent)
let createTryGrowOneSegmentFunction (bestNeighbours:Segmentation->Segment->Set<Segment>) (pixelMap:Coordinate->Segment) : (Segmentation->Coordinate->Segmentation) =
    
    let rec TryGrowOneSegment = fun segmentMap baseCoordinate ->
        
        // Determine the base and root segment
        let baseSegment = pixelMap baseCoordinate
        let rootSegment = findRoot segmentMap baseSegment

        // Find the neighbours of the root segment, convert it to a list
        let segmentA = bestNeighbours segmentMap rootSegment
        let segBList = segmentA |>Seq.toList


        // Recursive function that attempts to find mutual best neigbhours and merge them
        let rec findMutual n =

            if(segBList.Length - 1 >= n)then
                let segB = segBList.[n]

                // Go through each of Neighbour trying to be merged's best neighbours
                // And determine whether the original segment is a best neighbour
                let rec isMutual no =
                    if(segBList.Length - 1 >= no)then
                        let segB = segBList.[no]
                        let neighbours = bestNeighbours segmentMap segB

                        // If they are mutual best neighbours, merge them
                        if(neighbours.Contains rootSegment)then
                            let Parent = Parent(rootSegment, (findRoot segmentMap segB))
                           
                            let join (p:Map<'a,'b>) (q:Map<'a,'b>) = 
                              Map(Seq.concat [ (Map.toSeq p) ; (Map.toSeq q) ])

                            let test = Map [((findRoot segmentMap segB),Parent);(rootSegment,Parent)]

                            join segmentMap test
                            
                        else

                            isMutual (no + 1)
                            

                    else
                        findMutual (n + 1)
                isMutual 0
            else

                
                // If mutual best neighbours could not be found, and the original segment has
                // some best neighbours
                if(not (segBList.Length = 0))then

                    // find a pixel in the best neighbour and call the tryGrowSegment function using it
                    // in order to grow gradient descent like
                    let segB = segBList.[0]
                    let segC = findRoot segmentMap segB

                    let rec retPix seg = 
                     
                        match seg with
                        |Pixel(coordinates,colour) ->

                            TryGrowOneSegment segmentMap coordinates
                        |Parent(seg1,seg2) ->
                            
                            retPix(seg2)

                    retPix segC
                else
                    segmentMap
                

        findMutual 0
    TryGrowOneSegment
                    



            



// Try to grow the segments corresponding to every pixel on the image in turn 
// (considering pixel coordinates in special dither order)
let createTryGrowAllCoordinatesFunction (tryGrowPixel:Segmentation->Coordinate->Segmentation) (N:int) : (Segmentation->Segmentation) =

    // Grab the sequence for traversing the image from the DitherModule
    let sequence = DitherModule.coordinates N

    // Return a function that grows all coordinates
    fun segmentation ->
        
        // Continue growing until the final item in the sequence has been used
        let rec Grow count segment =
          
            if(count = (sequence|>Seq.length) - 1)then

                tryGrowPixel segment (sequence|>Seq.item count)
            else
                let d = tryGrowPixel segment (sequence|>Seq.item count)
                Grow (count + 1) d
            
        Grow 0 segmentation



// Keep growing segments as above until no further merging is possible
let createGrowUntilNoChangeFunction (tryGrowAllCoordinates:Segmentation->Segmentation) : (Segmentation->Segmentation) =
    
    // Returns a function that continuously grows a segmentation until no change is made
    fun segmentation ->
        
        let rec Grow segment =
            let current = tryGrowAllCoordinates segment
            if(current = segment)then
                current
            else
                Grow current

        Grow segmentation



// Segment the given image based on the given merge cost threshold, but only for the top left corner of the image of size (2^N x 2^N)
let segment (image:TiffModule.Image) (N: int) (threshold:float)  : (Coordinate -> Segment) =
   
    let pixelMap = createPixelMap image

    let neighbours = createNeighboursFunction pixelMap N

    let bestNeighbours = createBestNeighbourFunction neighbours threshold

    let growOne = createTryGrowOneSegmentFunction bestNeighbours pixelMap

    let growAll = createTryGrowAllCoordinatesFunction growOne N

    let growNoChange = createGrowUntilNoChangeFunction growAll

    let test = growNoChange Map.empty

    // Returns a function that given a coordinate, returns the root segment of the pixel of the coordinate
    fun coordinate ->

        let rootSegment = findRoot test (pixelMap coordinate)

        rootSegment

